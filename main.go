package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/costexplorer"
	"github.com/aws/aws-sdk-go-v2/service/costexplorer/types"
	"github.com/guptarohit/asciigraph"
)

func main() {
	cfg, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion("us-west-2"),
	)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}
	svc := costexplorer.NewFromConfig(cfg)

	now := time.Now()
	start := now.Add(-30 * 24 * time.Hour).Format(time.DateOnly)
	end := now.Format(time.DateOnly)

	groupKey := "USAGE_TYPE"

	resp, err := svc.GetCostAndUsage(context.TODO(), &costexplorer.GetCostAndUsageInput{
		Granularity: types.GranularityDaily,
		Metrics:     []string{"UNBLENDED_COST"},
		TimePeriod: &types.DateInterval{
			Start: &start,
			End:   &end,
		},
		GroupBy: []types.GroupDefinition{{
			Key:  &groupKey,
			Type: types.GroupDefinitionTypeDimension,
		}},
	})
	if err != nil {
		log.Fatalf("failed to get cost and usage, %v", err)
	}

	data := map[string]map[string]float64{}
	days := []string{}
	usageTypes := map[string]struct{}{}
	for _, t := range resp.ResultsByTime {
		day := map[string]float64{}
		data[*(t.TimePeriod.End)] = day          // pointer yolo
		days = append(days, *(t.TimePeriod.End)) // pointer yolo
		for _, g := range t.Groups {
			key := strings.Join(g.Keys, ":")
			usageTypes[key] = struct{}{}
			value, err := strconv.ParseFloat(*(g.Metrics["UnblendedCost"].Amount), 64) // pointer yolo
			if err != nil {
				log.Fatalf("failed to parse amount, %v", err)
			}
			day[key] = value
		}
	}

	allKeys := []string{}
	for k := range usageTypes {
		allKeys = append(allKeys, k)
	}
	for _, k := range allKeys {
		max := 0.0
		values := []float64{}
		for _, d := range days {
			v, _ := data[d][k]
			values = append(values, v)
			if v > max {
				max = v
			}
		}
		if max > 1.0 {
			color := asciigraph.Default
			if max > 5.0 {
				color = asciigraph.Red
			}
			graph := asciigraph.Plot(
				values,
				asciigraph.Height(5),
				asciigraph.Caption(k),
				asciigraph.SeriesColors(color),
			)
			fmt.Printf("\n" + graph + "\n")
		}
	}
	fmt.Printf("\n     DOLLARS PER DAY (30 days)\n\n")
}
